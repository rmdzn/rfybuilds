#!/bin/sh -e
#
# Slackware build script for palemoon.
#
# Copyright 2018, Ramdziana Feri Yustitianto
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

CWD=$(pwd)
if [ "TMP" = " " ]; then
    TMP=/tmp
fi

VERSION=28.1.0
PKG_VERSION=$VERSION

ARCH=${ARCH:-x86_64}

BUILD=${BUILD:-1_rfy}

APP=palemoon

PKG=$TMP/package-$APP

rm -rf $PKG
mkdir -p $TMP $PKG
rm -rf $TMP/$APP-$VERSION

cd $TMP || exit 1

tar jxvf $CWD/$APP-$VERSION.linux-x86_64.tar.bz2 || exit 1

cd $APP || exit 1

chown -R root:root .
chmod -R u+w,go+r-w,a-s .

mkdir -p $PKG/opt/$APP
mv * $PKG/opt/$APP
mkdir -p $PKG/usr/bin
ln -sf /opt/palemoon/palemoon $PKG/usr/bin/

mkdir -p $PKG/usr/doc/$APP-$VERSION
find $PKG/usr/doc/$APP-$VERSION -type f -exec chmod 644 {} \;
cat $CWD/$APP.SlackBuild > $PKG/usr/doc/$APP-$VERSION/$APP.SlackBuild

mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc

cd $PKG
/sbin/makepkg -l y -c n $TMP/$APP-$PKG_VERSION-$ARCH-$BUILD.tgz
